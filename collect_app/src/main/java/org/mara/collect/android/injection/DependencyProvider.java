package org.mara.collect.android.injection;

public interface DependencyProvider<T> {
    T provide();
}
