package org.mara.collect.android.widgets.interfaces;

/**
 * @author James Knight
 */
public interface FileWidget extends BinaryWidget {
    void deleteFile();
}
